// SPI Hexapod Interface
// Daniel McCarthy 12/19

#include "ProtocolInterface.hpp"

#include <wiringPiSPI.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

#include <stdint.h>
#include <stdlib.h>
#include <time.h>


SPIpacket Protocol::BuildPacket(uint8_t opcode, uint8_t arg) {
	SPIpacket pkt;
	pkt.opcode = opcode;
	pkt.arg = arg;

	//Populate Data with Sane Values
	for( uint8_t i = 0; i< 20; i++) {
		pkt.Data[i] = 36000; //1.5ms PWM
	}

	return pkt;
}

int SendData(SPIpacket *pkt) {
	wiringPiSPIDataRW(0, (uint8_t*)pkt, sizeof(SPIpacket));
	return 0;
}

PowerData ParsePowerData(SPIpacket *pkt) {
	//Grab single values
	float avgV = pkt->Data[3];
	float avgI = pkt->Data[2];
	
	//Convert to V
	avgV = (avgV/4096*3.3)*4.1333;      //V from divider
	avgI = ((avgI/4096*3.3)/200)/0.001; //I = V/R

	PowerData p;
	p.I = avgI;
	p.V = avgV;
	p.P = avgI*avgV;
	return p;
}

//Enable Servo n by setting the servo mask bit
SPIpacket Protocol::EnableServo(uint8_t n) {	
	SPIpacket m0 = BuildPacket(0x02, 0x00); //0x02 = Servo Mask
	m0.Data[0] = 0x0000;
	m0.Data[1] = 0x0000;
	if(n < 9) {
		m0.Data[0] = 0b1 << n; //0x01ff
	} else if(n < 19) {
		m0.Data[1] = 0b1 << n-7; //0x0ff8
	} else {
		printf("Index out of range");
	}
	return m0;
}


SPIpacket Protocol::EnableAllServos() {
	SPIpacket m0 = BuildPacket(0x02, 0x00); //0x02 = Servo Mask
	m0.Data[0] = 0xffff;
	m0.Data[1] = 0xffff;
	return m0;
}



//TODO: add calibration values to this
//Accepts angle in radians, sets timing based on calibration
void Protocol::SetServoAngle(uint8_t idx, float angle, SPIpacket *pkt) {
	//48000 = 2.0mS
	//36000 = 1.5mS
	//24000 = 1.0mS
	//12000 = 500uS
	if(angle > 0.0f && angle < M_PI) { //Make sure angle is sane
		
		//Calculate pulse width
		int32_t offset = (36000-ServoCalibration[idx]);
		//angle = M_PI-angle;

		//uint16_t pulse =  24000 + offset + 24000*(angle/M_PI);
		
		uint16_t pulse =  18200 + 40000*(angle/M_PI) - offset;

		//printf("a:%f p:%u o:%d\n", angle, pulse, offset);

		//Make sure pulse width is sane, don't want to damage the servos
		if(pulse > 15000 && pulse < 55000) {
			pkt->Data[idx] = pulse;
		}
	}
}

//TODO: load calibration file
void LoadCalibration() {
	return;
}

void Protocol::Init() {
	int fd = wiringPiSPISetup(0, 500000); //500kbps
	/* Data on rising edge is apparently unreliable, set data on falling edge
	 * STM32F103 CPOL = 0, CPHA = 1
	 * BCM2835_SPI_MODE1 CPOL = 0, CPHA = 1
	 */
	int spiMode = 1;
	ioctl(fd, SPI_IOC_WR_MODE, &spiMode);


	//Set Calibration Values
	//Leg 0
	ServoCalibration[10] = 36500;
	ServoCalibration[11] = 35500;
	ServoCalibration[12] = 33500;

	//Leg 1
	ServoCalibration[13] = 35000;
	ServoCalibration[14] = 34000;
	ServoCalibration[15] = 37000;

	//Leg 2
	ServoCalibration[0] = 37000;
	ServoCalibration[1] = 34000;
	ServoCalibration[2] = 35000;
	
	//Leg 3
	ServoCalibration[3] = 35500;
	ServoCalibration[4] = 39000;
	ServoCalibration[5] = 34000;
	
	//Leg 4
	ServoCalibration[6] = 36000;
	ServoCalibration[7] = 35500;
	ServoCalibration[8] = 39000;
	
	//Leg 5
	ServoCalibration[16] = 37000;
	ServoCalibration[17] = 38000;
	ServoCalibration[18] = 34000;
}


void Protocol::SetNeutral() {
	SPIpacket m0 = BuildPacket(0x01, 0x00);

	for(uint8_t i = 0; i < 20; i++) {
		m0.Data[i] = ServoCalibration[i];
	}
	SendData(&m0);

	m0 = EnableAllServos();
	SendData(&m0);
}


void Protocol::Send(Hexapod Hex) {
	//Build packet.
	SPIpacket pkt =	BuildPacket(0x01, 0x00); //0x01 = Set Servo


	// SPIpacket m0 = BuildPacket(0x02, 0x00); //0x02 = Servo Mask
	// m0.Data[0] = 0x0000;
	// m0.Data[1] = 0x0038; //Leg 0 only
	// SendData(&pkt);

	//Leg 0
	SetServoAngle(10, Hex.Legs[0].Ka, &pkt);  //K
	SetServoAngle(11, Hex.Legs[0].Ha, &pkt);  //T :')
	SetServoAngle(12, Hex.Legs[0].Ta, &pkt);  //H


	//Leg 1
	SetServoAngle(13, Hex.Legs[1].Ka, &pkt);  //K
	SetServoAngle(14, Hex.Legs[1].Ha, &pkt);  //T :')
	SetServoAngle(15, M_PI-Hex.Legs[1].Ta, &pkt);  //H


	//Leg 2
	SetServoAngle(0, Hex.Legs[2].Ka, &pkt);  //K
	SetServoAngle(1, Hex.Legs[2].Ha, &pkt);  //T :')
	SetServoAngle(2, Hex.Legs[2].Ta, &pkt);  //H

	//Leg 3
	SetServoAngle(3, Hex.Legs[3].Ka, &pkt);  //K
	SetServoAngle(4, Hex.Legs[3].Ha, &pkt);  //T :')
	SetServoAngle(5, M_PI-Hex.Legs[3].Ta, &pkt);  //H

	//Leg 4
	SetServoAngle(6, Hex.Legs[4].Ka, &pkt);  //K
	SetServoAngle(7, Hex.Legs[4].Ha, &pkt);  //T :')
	SetServoAngle(8, Hex.Legs[4].Ta, &pkt);  //H

	//Leg 5
	SetServoAngle(16, Hex.Legs[5].Ka, &pkt);  //K
	SetServoAngle(17, Hex.Legs[5].Ha, &pkt);  //T :')
	SetServoAngle(18, M_PI-Hex.Legs[5].Ta, &pkt);  //H

	

	SendData(&pkt);

	PowerData p = ParsePowerData(&pkt);
	printf("%fV @ %fA = %fW\n", p.V, p.I, p.P);
}

void Protocol::Time(float t) {
}

void Protocol::Disconnect() {
}