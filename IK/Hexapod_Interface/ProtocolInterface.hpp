#include <cstdio>
#include <math.h>
#include "../IK.hpp"


typedef struct SPIpacket {
	uint8_t opcode;
	uint8_t arg;
	uint16_t Data[20];
} SPIpacket;

typedef struct PowerData {
	float I;
	float V;
	float P;
} PowerData;


class Protocol {
public:
  void Init();
  void Send(Hexapod Hex);
  void Time(float t);
  void Disconnect();
  void SetNeutral();
private:
  SPIpacket BuildPacket(uint8_t opcode, uint8_t arg);
  SPIpacket EnableServo(uint8_t n);
  SPIpacket EnableAllServos();
  void SetServoAngle(uint8_t idx, float angle, SPIpacket *pkt);
  uint16_t ServoCalibration[20] = {0};
};