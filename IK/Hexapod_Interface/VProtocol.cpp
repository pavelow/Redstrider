#include "ProtocolInterface.hpp"
//#include <iostream>
#include <string>
#include <stdio.h>
#include <math.h>

extern "C" {
    #include "remoteApi/extApi.h"
    #include "include/v_repConst.h"
}

simxInt ClientID;
simxInt LegP[6];
simxInt LegH[6];
simxInt LegK[6];


simxFloat CalP[6] = {0.0f};
simxFloat CalH[6] = {0.0f};
simxFloat CalK[6] = {0.0f};

float t = 0;


simxInt opmode_blocking = 0x010000;
simxInt opmode_streaming = 0x020000;

void Protocol::Init() {
    ClientID = simxStart("127.0.0.1", 45566, false, false, 500, 5);
    printf("Connected: %d\n", ClientID);
    // Get Handles for all the joints
    for (uint i = 0; i < 6; i++) {
        printf("Getting Handle: %d/5\n", i);
        std::string a;
        a = "Pelvis_" + std::to_string( i ) + "#";
        simxGetObjectHandle(ClientID, a.c_str(), &LegP[i], opmode_blocking);

        a = "ip_" + std::to_string( i ) + "#";
        simxGetObjectHandle(ClientID, a.c_str(), &LegH[i], opmode_blocking);

        a = "Knee_" + std::to_string( i ) + "#";
        simxGetObjectHandle(ClientID, a.c_str(), &LegK[i], opmode_blocking);
    }
    for(uint i=0; i < 6; i++) {
      simxFloat a = 0.0f;
      simxGetJointPosition(ClientID, LegP[i], &CalP[i], opmode_blocking);
      simxGetJointPosition(ClientID, LegH[i], &CalH[i], opmode_blocking);
      simxGetJointPosition(ClientID, LegK[i], &CalK[i], opmode_blocking);
      //printf("%.2f\n",CalP[i]);
    }
}


float Flip(int n) {
    if(n == 0 || n == 2) { //n == 4 ||
      return -1;
    } else {
      return 1;
    }
}


float PFlip(int n) {
  if(n == 4) { //|| n == 2 || n == 0
    return -1;
  } else {
    return 1;
  }
}

void Protocol::Time(float _t) {
  t = _t;
}
// Knee, Hip Inverted across 0 2 4
// Even -H, +K
// Odd  +H, -K

void Protocol::Disconnect() {
  simxFinish(ClientID);
}



void Protocol::Send(Hexapod Hex) {
  for(uint i=0; i < 6; i++) {
    //print "Sending L{3} P:{0} H:{1} K:{2}".format(Leg.Ta *Rconversion - pi/2, Leg.Ha *Rconversion - pi/2, Leg.Ka *Rconversion - pi/2, i)

    simxSetJointTargetPosition(ClientID, LegP[i], (CalP[i] + PFlip(i)*Flip(i)*(M_PI - Hex.Legs[i].Ta - M_PI/2)) , opmode_streaming); //- M_PI/2

    simxSetJointTargetPosition(ClientID, LegH[i], (CalH[i] + Flip(i)*(Hex.Legs[i].Ha - M_PI/2)), opmode_streaming);// //- M_PI/2 + M_PI/2

    simxSetJointTargetPosition(ClientID, LegK[i], (CalK[i] + Flip(i)*(M_PI-Hex.Legs[i].Ka - M_PI/2)), opmode_streaming);// //Flip(i)*

    //printf("%.2f\n",Hex.Legs[i].Ka);
  }
}