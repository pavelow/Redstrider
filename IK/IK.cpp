#include "IK.hpp"
#include <stdio.h>


//Constructor
Leg::Leg(uint8_t _index) {
  index = _index;
}

Leg::Leg() {
}

LegAngles Leg::GetAngles() {
  LegAngles l;
  l.Ha = Ha;
  l.Ka = Ka;
  l.Ta = Ta;
  return l;
}

//2D IK
void Leg::Set2D(glm::vec2 Target) {
  //printf("2D called with X=%f Y=%f\n", Target.x, Target.y);
  float Dl = sqrt(pow(Target.x,2) + pow(Target.y,2));// #Distance to point
  float Da;

	if(Target.x != 0) { //Avoid division by zero
    Da = atan(Target.y/Target.x); //Angle to point
  } else {
    Da = M_PI;
  }

	//Find angles, by law of cosines c^2 = a^2 + b^2 -2ab cos(C)
	float cKa = (pow(Hl,2) + pow(Kl,2) - pow(Dl,2))/(2*Hl*Kl); // cos(Ka)
	if(abs(cKa) <= 1) { //not Impossible K Position
    Ka = M_PI - (acos(cKa));
  }

  //Validate Ha
	float cHa = (pow(Hl,2) + pow(Dl,2) - pow(Kl,2))/(2*Hl*Dl); // cos(Ha)

	if (abs(cHa) <= 1){ // not Impossible H Position
		if(Target.x > 0) {
      Ha = M_PI/2 + ((acos(cHa) - Da));
    } else if(Target.x == 0) {
			Ha = ((acos(cHa) - Da));
    } else {
			Ha = ((acos(cHa) - Da)) - M_PI/2;
    }
		Ha += KneeOffsetAngle;
  }
}

//3D IK in offset space
void Leg::Set3D(glm::vec3 Target) {
	//Convert to local Coords

  //Convert Target to Local Coords
  //Pos = Target;
  //Target -= Global;
  Target -= Offset;
  //printf("L_Leg:%d x:%0.2f, y:%0.2f, z:%0.2f\n", index, Target.x, Target.y, Target.z);
  //printf("G_Leg:%d x:%0.2f, y:%0.2f, z:%0.2f\n", index, Global.x, Global.y, Global.z);
  //Local = Target;

	//Z *= -1 # I have no idea why... For 8266
  //Pos.z *= -1;

  //If Leg is odd, it's on the other side, so we need to invert some stuff
	if(index % 2 == 0) {
    Target.x *= -1;
  }

	float theta = 0.0f;
	if(Target.x !=0){
    theta = atan(Target.z/Target.x);// Angle Compensated
    //printf("%f\n", theta);
  }
  Ta = (M_PI/2)-theta;

	float leng  = sqrt(pow(Target.z,2) + pow(Target.x,2)); //Leng Compensated
	Set2D(glm::vec2(((Target.x > 0.0f) ? 1.0f : -1.0f)*leng-Tl , Target.y));
}



//Constructor
Hexapod::Hexapod(float Height, float LegLen) {
  for(uint i = 0; i< 6; i++) {
    Legs[i] = Leg(i);
  }
  SetOffsets(Height, LegLen);
}

void Hexapod::SetOffsets(float Height, float LegLen) {

  //Offsets are positions of leg roots relative to the center of the body
  //Set X Offsets
  for(uint i = 0; i < 6; i++) {
    if(i % 2 > 0) {
      Legs[i].Offset.x = 2.156;
    } else {
      Legs[i].Offset.x = -2.156;
    }
    //Legs[i].Offset.y = Height;
    Legs[i].Global.y = Height;
  }

  //Center legs
  // Legs[2].Offset.x = 3;
  // Legs[3].Offset.x = -3;


  //Set Z Offsets for front and rear legs
  Legs[0].Offset.z = LegLen*0.707;
  Legs[1].Offset.z = LegLen*0.707;

  Legs[4].Offset.z = -LegLen*0.707;
  Legs[5].Offset.z = -LegLen*0.707;

  Legs[2].Offset.z = 0;
  Legs[3].Offset.z = 0;

  //Set Global coordinates
  Legs[2].Global.x = -LegLen + Legs[2].Offset.x;
  Legs[3].Global.x = LegLen + Legs[3].Offset.x;

  //Front Legs
  Legs[0].Global.x = Legs[0].Offset.x - LegLen*0.707;
  Legs[0].Global.z = Legs[0].Offset.z + LegLen*0.707;
  Legs[1].Global.x = Legs[1].Offset.x + LegLen*0.707;
  Legs[1].Global.z = Legs[1].Offset.z + LegLen*0.707;

  //Rear Legs
  Legs[5].Global.x = Legs[5].Offset.x + LegLen*0.707;
  Legs[5].Global.z = Legs[5].Offset.z - LegLen*0.707;
  Legs[4].Global.x = Legs[4].Offset.x - LegLen*0.707;
  Legs[4].Global.z = Legs[4].Offset.z - LegLen*0.707;
}


void Hexapod::Translate(uint8_t Leg, glm::vec3 Dist) {
  TransRot(Leg, Dist, glm::vec3(0,0,0));
}

//Rotates foot about body origin
void Hexapod::Rotate(uint8_t Leg, glm::vec3 Angle) {
  TransRot(Leg, glm::vec3(0,0,0), Angle);
}

void Hexapod::TransRot(uint8_t Leg, glm::vec3 Dist, glm::vec3 Angle) {

  glm::mat4 rotationMat = glm::eulerAngleYXZ(Angle.y, Angle.x, Angle.z);
  glm::vec3 Pos = glm::vec4(Legs[Leg].Global, 1.0) * rotationMat;
  Pos += Dist;


  Legs[Leg].Set3D(Pos); //Give Global Coord

}