#include <cstdio>
#include "IK.hpp"
#include "joystick/xControllerUDP.hpp"
#include "Hexapod_Interface/ProtocolInterface.hpp"
#include <unistd.h>
#include <glm/glm.hpp>
#include <vector>
#include <cmath>

//Globals :')
//std::vector<uint8_t> GroundLegs{1,2,5}; //All legs start in the grounded set
glm::vec3 FootPosition[6];   //Current foot positions
glm::vec3 Rotations[6];     //Currenty foot rotations
glm::vec3 Rotation = glm::vec3(0,0,0);

const float StepThreshold = 0.2; //Threshold at which legs move with ground vs move with
bool Stepping[6];  //Currently stepping legs
float StepProgress[6]; //Step Progress per leg
glm::vec3 StepTarget[6]; //Target foot positions for stepping
glm::vec3 StepRotationTarget[6];


void Step(uint8_t Leg, glm::vec3 Tran, glm::vec3 Rot) {
  StepTarget[Leg] = Tran;
  StepRotationTarget[Leg] = Rot;
  Stepping[Leg] = true;
  StepProgress[Leg] = 0.0f;
}


int main(int argc, char ** argv) {

  //Initialise foot position vectors
  for(uint8_t i = 0; i < 6; i++) {
    FootPosition[i] = glm::vec3(0,0,0);
    Stepping[i] = false;
    Rotations[i] = glm::vec3(0,0,0);
  }
  //Init Hexapod IK Model with foot offset = height, radius
  Hexapod Hex = Hexapod(6,8);

  //Init Joystick input
  JoyStick J = JoyStick();

  //Init Hexapod protocol
  Protocol P = Protocol();
  P.Init();

  uint32_t t = 0;




  for(;;) {
    //Get control input
    glm::vec2 Translate = glm::vec2(J.GetLeftStick().y, J.GetLeftStick().x); //Populate Translate vector
    Translate.x *= -1; //Invert X axis
    Rotation = glm::vec3(0, J.GetRightStick().x*0.1 ,0);

    //Move Grounded Legs
    for(uint8_t i = 0; i < 6; i++) {
      if(!Stepping[i]) {
        //FootPosition[i] += glm::vec3(Translate.y*-0.5, 0, Translate.x*-0.5);
        FootPosition[i] += glm::vec3(Translate.y*-0.1, 0, Translate.x*-0.1);
        FootPosition[i].y = 0;
        Rotations[i] += Rotation*glm::vec3(0.05,0.05,0.05);
        Hex.TransRot(i, FootPosition[i], Rotations[i]);// Rotations[GroundLegs[i]]); //Need to move linearly
      }
    }

    //Move Stepping Legs
    for(uint8_t i = 0; i < 6; i++) {
      if(Stepping[i] == true) {
        //Height
        FootPosition[i].y = ((pow((2*StepProgress[i]-1),2) - 1)); //Parabola
        printf("Foot.y: %f\n", FootPosition[i].y);

        //Get unit vector
        glm::vec3 unit =  FootPosition[i] - StepTarget[i];
        unit /= glm::length(unit);

        //Translation
        FootPosition[i].x -= unit.x*0.6;
        FootPosition[i].z -= unit.z*0.6;
        // FootPosition[i].x = StepTarget[i].x;
        // FootPosition[i].z = StepTarget[i].z;

        glm::vec3 Runit =  Rotations[i] - StepRotationTarget[i];
        Runit /= glm::length(unit);



        Rotations[i].y -= Runit.y*0.2;

        Hex.TransRot(i, FootPosition[i], Rotations[i]);// Rotations[GroundLegs[i]]); //Need to move linearly

        StepProgress[i] += 0.1;
        //printf("SProgress: %f\n", StepProgress[i]);
        if(StepProgress[i] > 1.0f) {
          //printf("rstStep\n");
          Stepping[i] = false;
          StepProgress[i] = 0.0f;
        }
      }
    }


    // 1 Leg Ripple Gait
    static uint8_t leg_idx = 0;
    bool steppingGlobal = false;
    for(uint8_t i = 0; i < 6; i++) {
      if(Stepping[i] == true) {
        steppingGlobal = true;
      }
    }

    if(!steppingGlobal) {
      glm::vec3 Target = glm::vec3(0,-1,0) - glm::vec3(Translate.y*-1, 0, Translate.x*-1);
      glm::vec3 RotTarget = -Rotation*glm::vec3(0.05,0.05,0.05);
      Step(leg_idx, Target, RotTarget);

      leg_idx++;

      if(leg_idx > 5) {
        leg_idx = 0;
      }
    }








    P.Send(Hex); //Send new hexapod state

    usleep(1000*20); //20ms sleep
    t += 1;//0.01;

    //printf("%0.2f, R:%f\n", glm::length(FootPosition[GroundLegs[0]]), Rotations[GroundLegs[0]].y);
    LegAngles a = Hex.Legs[0].GetAngles();
    printf("Target\n X=%.02f\n Y=%.02f\n Z=%.02f\n\n", StepTarget[0].x, StepTarget[0].y, StepTarget[0].z);
    printf("Coords\n X=%.02f\n Y=%.02f\n Z=%.02f\n\n", FootPosition[0].x, FootPosition[0].y, FootPosition[0].z);
    printf("Angles Radians\n Ha=%.02f\n Ka=%.02f\n Ta=%.02f\n\n", a.Ha, a.Ka, a.Ta);
    printf("Angles Degrees\n Ha=%.02f\n Ka=%.02f\n Ta=%.02f\n\n", a.Ha*180/M_PI, a.Ka*180/M_PI, a.Ta*180/M_PI);

  }
}