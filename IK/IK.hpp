#pragma once
#include <cmath>
#include <cstdint>
#include <glm/glm.hpp>
#include <glm/gtx/euler_angles.hpp>

//Lengths in cm
#define Kl 9.45  //Knee Length,  9.45 //9.45 with feet, 9.1 Without feet
#define Hl 4.312 //Hip Length
#define Tl 4.1 //Thigh Length
#define KneeOffsetAngle -atan(2.05/Kl)///(M_PI/180)

typedef struct LegAngles {
  float Ka, Ha, Ta;
} LegAngles;

class Leg {
public:
  Leg();
  Leg(uint8_t index);
  LegAngles GetAngles();
  void Set2D(glm::vec2 Target); //2D IK Solve, Knee, Thigh
  void Set3D(glm::vec3 Target); //3D IK Solve, All
  void SetOffset(glm::vec3 Offset); //Set offset
  glm::vec3 Offset = glm::vec3(0,0,0);   //Leg Coordinate Offsets relative to root of leg
  glm::vec3 Global = glm::vec3(0,0,0);   //Coordinates, in body space (All further Transformations are relative to these)
  glm::vec3 Local = glm::vec3(0,0,0);    //Foot Coordinate in Local space from root of leg
  //Angles in degrees, gives initial pose
  float Ka = M_PI/2; //Knee Angle
  float Ha = M_PI/2; //Hip Angle
  float Ta = M_PI/2; //Pelvic Angle

private:
  uint8_t index; //Index of leg


};

class Hexapod {
public:
  Hexapod(float Height, float LegDist); //Height of Body & Protrusion from body of leg at zero position
  void Translate(uint8_t Leg, glm::vec3 Dist);
  void Rotate(uint8_t Leg, glm::vec3 Angle); //Rotates foot about body origin
  void TransRot(uint8_t Leg, glm::vec3 Dist, glm::vec3 Angle);
  Leg Legs[6]; //Array of legs
private:
  void SetOffsets(float Height, float LegDist);
};