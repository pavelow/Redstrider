#! /usr/bin/bash
gcc -c IK.cpp -o IK.o
gcc -c test_IK_Hex.cpp -o testIKHex.o
gcc -c Gait.cpp -o Gait.o
gcc -c Gait_Step.cpp -o Gait_Step.o
#gcc -c joystick/xController.cpp -o joystick/js.o

cd Hexapod_Interface
make
cd ..

g++ Hexapod_Interface/extApi.o Hexapod_Interface/extApiPlatform.o Hexapod_Interface/VProtocol.o IK.o joystick/js.o testIKHex.o -o TestIK -lpthread -lm

g++ Hexapod_Interface/extApi.o Hexapod_Interface/extApiPlatform.o Hexapod_Interface/VProtocol.o IK.o joystick/js.o Gait.o -o Gait -lpthread -lm

g++ Hexapod_Interface/extApi.o Hexapod_Interface/extApiPlatform.o Hexapod_Interface/VProtocol.o IK.o joystick/js.o Gait_Step.o -o Gait_Step -lpthread -lm

#g++ testIKHex.o IK.o joystick/js.o -lm -lSDL2 -o testIKHex

