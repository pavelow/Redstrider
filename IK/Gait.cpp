#include <cstdio>
#include "IK.hpp"
#include "joystick/xControllerUDP.hpp"
#include "Hexapod_Interface/ProtocolInterface.hpp"
#include <unistd.h>
#include <csignal>
#include <glm/glm.hpp>
#include <vector>
#include <cmath>

void signalHandler( int signum ) {
   exit(signum);
}


//Globals :')
std::vector<uint8_t> GroundLegs{1,2,5}; //All legs start in the grounded set
std::vector<uint8_t> LiftedLegs{0,3,4};
glm::vec3 FootPosition[6]; //Foot positions
glm::vec3 Rotations[6];
glm::vec3 Rotation = glm::vec3(0,0,0);
float LegProx[6]; //Proximity of leg to other leg -- Will need to know direction of travel of closest leg too --

int main(int argc, char ** argv) {
  signal(SIGINT, signalHandler);

  //Initialise foot position vectors
  for(uint8_t i = 0; i < 6; i++) {
    FootPosition[i] = glm::vec3(0,0,0);
    Rotations[i] = glm::vec3(0,0,0);
  }
  Hexapod Hex = Hexapod(6,8); //Init Hexapod IK height, len
  JoyStick J = JoyStick();
  Protocol P = Protocol();
  P.Init();

  uint t = 0;

  for(;;) {
    glm::vec2 Translate = glm::vec2(J.GetLeftStick().y, J.GetLeftStick().x);
    Translate.x *= -1; //Invert X axis
    bool ChangeFlag = true;

    Rotation = glm::vec3(0, J.GetRightStick().x ,0);

    //Move Grounded Legs
    for(uint8_t i = 0; i < GroundLegs.size(); i++) {
      FootPosition[GroundLegs[i]] += glm::vec3(Translate.y*-0.5, 0, Translate.x*-0.5);
      FootPosition[GroundLegs[i]].y = 0;
      Rotations[GroundLegs[i]] += Rotation*glm::vec3(0.05,0.05,0.05);
      Hex.TransRot(GroundLegs[i], FootPosition[GroundLegs[i]], Rotations[GroundLegs[i]]); //Need to move linearly
    }

    //Move Lifted Legs
    for(uint8_t i = 0; i < LiftedLegs.size(); i++) {
      //FootPosition[LiftedLegs[i]] -= glm::vec3(Translate.y*-0.1, 0, Translate.x*-0.1);
      glm::vec3 Target = glm::vec3(0,-1,0) - glm::vec3(Translate.y*-1, 0, Translate.x*-1);
      FootPosition[LiftedLegs[i]] = Target;
      //FootPosition[LiftedLegs[i]].y = 1;
      Rotations[LiftedLegs[i]] = glm::vec3(0,0,0) - Rotation*glm::vec3(0.1,0.1,0.1);
      Hex.TransRot(LiftedLegs[i], FootPosition[LiftedLegs[i]], Rotations[LiftedLegs[i]]); //Need to move linearly
    }

    //Promote/Demote Legs to/from Lifted/Grounded
    for(uint8_t i = 0; i < 6; i++) {
    }


    if(glm::length(FootPosition[GroundLegs[0]]) > 2 || fabs(Rotations[GroundLegs[0]].y) > 0.2)  {
      if(ChangeFlag) {
        printf("Change\n");
        std::vector<uint8_t> d = GroundLegs;
        GroundLegs = LiftedLegs;
        LiftedLegs = d;
        ChangeFlag = false;
      }
    }

    // if(t > 25) {
    //   printf("Change\n");
    //   std::vector<uint8_t> d = GroundLegs;
    //   GroundLegs = LiftedLegs;
    //   LiftedLegs = d;
    //   ChangeFlag = false;
    //   t = 0;
    // }



    if (glm::length(FootPosition[GroundLegs[0]]) < 0.5 || fabs(Rotations[GroundLegs[0]].y) < 0.07) {
      ChangeFlag = true;
    }

    printf("%0.2f, R:%f\n", glm::length(FootPosition[GroundLegs[0]]), Rotations[GroundLegs[0]].y);



    P.Send(Hex); //Send new hexapod state

    usleep(1000*20); //20ms
    t += 0.01;
  }



  //LegAngles a = IK.GetAngles();
  // printf("Angles Radians\n Ha=%.02f\n Ka=%.02f\n Ta=%.02f\n\n", a.Ha, a.Ka, a.Ta);
  // printf("Angles Degrees\n Ha=%.02f\n Ka=%.02f\n Ta=%.02f\n\n", a.Ha*180/M_PI, a.Ka*180/M_PI, a.Ta*180/M_PI);

}