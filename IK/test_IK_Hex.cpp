#include <cstdio>
#include "IK.hpp"
#include "joystick/xControllerUDP.hpp"
#include "Hexapod_Interface/ProtocolInterface.hpp"
#include <unistd.h>
#include <csignal>
#include <glm/glm.hpp>

void signalHandler( int signum ) {
   exit(signum);
}

int main(int argc, char ** argv) {
  signal(SIGINT, signalHandler);
  Hexapod Hex = Hexapod(8,8); //Init Hexapod IK height, len
  JoyStick J = JoyStick();
  Protocol P = Protocol();
  P.Init();

  glm::vec2 R;

  float t = 0;

  for(;;) {
    for(uint i = 0; i < 6; i++) {
      //Hex.Rotate(i, glm::vec3(J.GetRightStick().y, 0, J.GetRightStick().x*0.1));
      //Hex.Translate(i, glm::vec3(2*J.GetRightStick().x, 0, 2*J.GetRightStick().y));
      //Hex.TransRot(i, glm::vec3(-J.GetLeftStick().y*0, 0, J.GetLeftStick().x*0), glm::vec3(J.GetRightStick().y*0.2, J.GetLeftStick().y*0.2, J.GetRightStick().x*0.2));
      Hex.TransRot(i, glm::vec3(-J.GetLeftStick().x*1, 0, J.GetLeftStick().y*1), glm::vec3(J.GetRightStick().y*0.2, 0*0.2, J.GetRightStick().x*0.2));
      //Hex.Translate(i, glm::vec3(0, 0, 0));
      //Hex.Translate(i, glm::vec3(0, sin(t), 0));
      //Hex.Legs[1].Set2D(glm::vec2(-2.156, sin(t)));



      // if(i % 2 > 0) {
      //   Hex.Legs[i].Set3D(glm::vec3(5 + J.GetRightStick().x*-2,  7 + J.GetLeftStick().x*-2, J.GetRightStick().y*2));//9.45
      // }
      // if(i % 2 == 0) {
      //   Hex.Legs[i].Set3D(glm::vec3(-5 + J.GetRightStick().x*-2,  7 + J.GetLeftStick().x*-2, J.GetRightStick().y*2));//9.45
      // }
    }

    //Hex.Legs[1].Set3D(glm::vec3(4.1 + J.GetRightStick().y*2,  9.45 + J.GetRightStick().x*2, J.GetLeftStick().x));//9.45

    //Hex.Rotate(0, glm::vec3(1, J.GetRightStick()));
    //P.Time(t);
    P.Send(Hex);

    usleep(1000*20); //20ms
    t += 0.01;

    LegAngles a = Hex.Legs[0].GetAngles();
    printf("Angles Radians\n Ha=%.02f\n Ka=%.02f\n Ta=%.02f\n\n", a.Ha, a.Ka, a.Ta);
    printf("Angles Degrees\n Ha=%.02f\n Ka=%.02f\n Ta=%.02f\n\n", a.Ha*180/M_PI, a.Ka*180/M_PI, a.Ta*180/M_PI);
  }



  //LegAngles a = IK.GetAngles();
  // printf("Angles Radians\n Ha=%.02f\n Ka=%.02f\n Ta=%.02f\n\n", a.Ha, a.Ka, a.Ta);
  // printf("Angles Degrees\n Ha=%.02f\n Ka=%.02f\n Ta=%.02f\n\n", a.Ha*180/M_PI, a.Ka*180/M_PI, a.Ta*180/M_PI);

}
