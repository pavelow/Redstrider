#include <cstdio>
#include "IK.hpp"

int main(int argc, char ** argv) {
  Leg IK = Leg(0);
  //IK.Set3D(glm::vec3(-6.0f,-0.0f, 0.0f));
  //IK.Set2D(glm::vec2(-6.0f,0.0f));
  LegAngles a = IK.GetAngles();

  printf("Angles Radians\n Ha=%.02f\n Ka=%.02f\n Ta=%.02f\n\n", a.Ha, a.Ka, a.Ta);
  printf("Angles Degrees\n Ha=%.02f\n Ka=%.02f\n Ta=%.02f\n\n", a.Ha*180/M_PI, a.Ka*180/M_PI, a.Ta*180/M_PI);

  //IK.Set3D(glm::vec3(8.4119f,9.45f,0.0f));
  IK.Set3D(glm::vec3(4.1f,9.45f,1.0f));
  a = IK.GetAngles();

  printf("Angles Radians\n Ha=%.02f\n Ka=%.02f\n Ta=%.02f\n\n", a.Ha, a.Ka, a.Ta);
  printf("Angles Degrees\n Ha=%.02f\n Ka=%.02f\n Ta=%.02f\n\n", a.Ha*180/M_PI, a.Ka*180/M_PI, a.Ta*180/M_PI);

}
