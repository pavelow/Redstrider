#include "xController.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_joystick.h>

JoyStick::JoyStick() {
  //Initialize
  SDL_InitSubSystem(SDL_INIT_JOYSTICK);
  //SDL_SetHint(SDL_HINT_NO_SIGNAL_HANDLERS, "1");
  //SDL_InitSubSystem(SDL_INIT_EVENTS);

  //Try to open Joystick
  if (SDL_NumJoysticks() > 0) {
      // Open joystick
      joy = SDL_JoystickOpen(0);

      if (joy) {
          printf("Opened Joystick 0\n");
          printf("Name: %s\n", SDL_JoystickNameForIndex(0));
          printf("Number of Axes: %d\n", SDL_JoystickNumAxes(joy));
          printf("Number of Buttons: %d\n", SDL_JoystickNumButtons(joy));
          printf("Number of Balls: %d\n", SDL_JoystickNumBalls(joy));
      } else {
          printf("Couldn't open Joystick 0\n");
      }
  }
}

JoyStick::~JoyStick() {
  // Close if opened
  if (SDL_JoystickGetAttached(joy)) {
      SDL_JoystickClose(joy);
      //SDL_QuitSubSystem(SDL_INIT_JOYSTICK);
      SDL_Quit();
  }
}

glm::vec2 JoyStick::GetLeftStick() {
  SDL_JoystickUpdate();
  Sint16 x_move = SDL_JoystickGetAxis(joy, 1);
  Sint16 y_move = SDL_JoystickGetAxis(joy, 0);
  return glm::vec2(x_move/pow(2,15), y_move/pow(2,15));
}


glm::vec2 JoyStick::GetRightStick() {
  SDL_JoystickUpdate();
  Sint16 x_move = SDL_JoystickGetAxis(joy, 3);
  Sint16 y_move = SDL_JoystickGetAxis(joy, 4);
  return glm::vec2(x_move/pow(2,15), y_move/pow(2,15));
}