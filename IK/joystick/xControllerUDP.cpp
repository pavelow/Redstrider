//#include "xController.hpp" //Interface
#include "xControllerUDP.hpp" //Interface

#define PORT 42424
#define MAXLINE 1024

typedef struct rxmsg{
    float lx;
    float ly;
    float rx;
    float ry;
}rxmsg;


float ReverseFloat( const float inFloat ) {
   float retVal;
   char *floatToConvert = ( char* ) & inFloat;
   char *returnFloat = ( char* ) & retVal;

   // swap the bytes into a temporary buffer
   returnFloat[0] = floatToConvert[3];
   returnFloat[1] = floatToConvert[2];
   returnFloat[2] = floatToConvert[1];
   returnFloat[3] = floatToConvert[0];

   return retVal;
}

//Constructor
JoyStick::JoyStick() {

    //Creating socket file descriptor
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket creation failure");
        exit(EXIT_FAILURE);
    }

    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(PORT);

    if( bind(sockfd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    //Start recv thread
    recvThread = new std::thread(&JoyStick::recv, this);
}


JoyStick::~JoyStick() {
    //Kill thread
    recvThread->join();
}

//Recieve loop
void JoyStick::recv() {
    
    for (;;) {
        unsigned len;
        int n;
        len = sizeof(cliaddr);

        n = recvfrom(sockfd, (char *)buffer, 16, MSG_WAITALL, (struct sockaddr *) &cliaddr, &len);
        
        rxmsg* b = (rxmsg*)&buffer;
        
        Right.x = ReverseFloat(b->rx);
        Right.y = ReverseFloat(b->ry);

        Left.x = ReverseFloat(b->lx);
        Left.y = ReverseFloat(b->ly);

        std::this_thread::sleep_for (std::chrono::milliseconds(10)); //Sleep for 10ms so we don't hog cycles
    }
}

glm::vec2 JoyStick::GetRightStick() {
    return Right;
}

glm::vec2 JoyStick::GetLeftStick() {
    return Left;
}