#include <glm/glm.hpp>
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <thread>

class JoyStick {
public:
  JoyStick();
  ~JoyStick();
  glm::vec2 GetRightStick();
  glm::vec2 GetLeftStick();

private:
  void recv();
  int sockfd;
  char buffer[16];
  struct sockaddr_in servaddr, cliaddr;
  glm::vec2 Right = glm::vec2(0,0);
  glm::vec2 Left = glm::vec2(0,0);
  std::thread *recvThread;
};