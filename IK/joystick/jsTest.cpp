#include <stdio.h>
#include <csignal>
#include <unistd.h>
#include "xControllerUDP.hpp"

void signalHandler( int signum ) {
   exit(signum);
}

int main(int argc, char** argv) {
  signal(SIGINT, signalHandler);

  JoyStick J = JoyStick();
  glm::vec2 R;


  for(;;) {
    R = J.GetLeftStick();
    printf("Left x=%.02f, y=%.02f\n", R.x, R.y);
    R = J.GetRightStick();
    printf("Right x=%.02f, y=%.02f\n", R.x, R.y);

    usleep(1000*100);
  }
}
