#include <glm/glm.hpp>

class JoyStick {
public:
  JoyStick();
  ~JoyStick();
  glm::vec2 GetRightStick();
  glm::vec2 GetLeftStick();
};