# STM32 Code
The code here is for implementing servo signal generation on the STM32 Blue-Pill Board.

## Building
See this book https://cs.indiana.edu/~geobrown/book.pdf

Will need: `git clone git://github.com/geoffreymbrown/STM32-Template.git`
And: https://www.st.com/en/embedded-software/stsw-stm32054.html

Point libroot in Makefile.common to the ST library `LIBROOT=<xyz>/STM32F10x_StdPeriph_Lib_V3.5.0`

Install
- `arm-none-eabi-gcc`
- `arm-none-eabi-binutils`
- `arm-none-eabi-newlib`
- `arm-none-eabi-gdb`

## Flashing
- `stlink` -> https://github.com/texane/stlink

## Pin Assignments

### SPI
PB12 NSS2
PB13 SCK2
PB14 MISO2
PB15 MOSI2

### UART
PB11 RX3
PB10 TX3

### Servo
PA0 0
PA1 1
PA2 2
PA3 3
PA4 4
PA5 5
PA6 6
PA7 7
PA8 8
PA9 9

PB0 10
PB1 11

PB2 12 (BOOT1) //Disabled

PB3 13
PB4 14
PB5 15
PB6 16
PB7 17
PB8 18
PB9 19

