/*
2019 - Daniel McCarthy
Servo PWM Generation & SPI Protocol
*/

#include <stm32f10x.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_tim.h>
#include <stm32f10x_spi.h>
#include <stm32f10x_usart.h>
#include <misc.h> //NVIC
#include <stdbool.h>
#include <stdio.h>
#include <math.h>

//#define DEBUG_UART


//Servo timings
static volatile uint16_t ServoValues[20];
static volatile uint32_t Milliseconds = 0;
static volatile unsigned char rx = 0;
static volatile bool Recv = false;


void GPIO_Setup() {
	// Enable Peripheral Clocks
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE); // Enable GPIO Port A,B I2C1

	// Configure Pins
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit (&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = 0x03ff; //PA0-PA9, PB0-PB9
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz; //Slew rate
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = 0x03fb; //PB0-1,5-9 0x03e3
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void Timer_Setup() {
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;

	//Enable Timer Clock
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	//Configure Clocks - 72,000,000 Hz system clock, 50Hz Timer
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler = SystemCoreClock/24000000-1; // 24.0Mhz = 48000 * 500Hz
	TIM_TimeBaseStructure.TIM_Period = 48000; // 500Hz, 2ms
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

	//Setup Output Compare
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OC2Init(TIM2, &TIM_OCInitStructure);

	//Configure TIM2 Interrupt on NVIC
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1); // 1 bits priority, 3 bits subgroup
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1; //Low priority
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	//Enable Timer Interrupts
	TIM_ITConfig(TIM2, TIM_IT_CC1 | TIM_IT_CC2 | TIM_IT_Update, ENABLE);

	//Enable Timer
	TIM_Cmd(TIM2, ENABLE);
}

void TIM2_IRQHandler(void) {
	static uint8_t index = 0;

	//Handle Timer Reset
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) {
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update); //Clear the interrupt bit

		//Turn on all the things
		GPIO_Write(GPIOA, 0x03ff & 0x01 << index);
		TIM2->CCR1 = ServoValues[index]; //Set Pulse Time
		GPIO_Write(GPIOB, 0x03fb & 0x01 << index); //0x03e3
		TIM2->CCR2 = ServoValues[index+10]; //Set Pulse Time

		index++;
		if(index == 10) {
			index = 0;
		}
	}

	//Handle CC1
	if (TIM_GetITStatus(TIM2, TIM_IT_CC1) != RESET) {
		TIM_ClearITPendingBit(TIM2, TIM_IT_CC1); //Clear the interrupt bit
		GPIO_Write(GPIOA, 0x0000); //OFF
	}

	//Handle CC2
	if (TIM_GetITStatus(TIM2, TIM_IT_CC2) != RESET) {
		TIM_ClearITPendingBit(TIM2, TIM_IT_CC2); //Clear the interrupt bit
		GPIO_Write(GPIOB, 0x0000); //OFF
	}
}

// setup USART3 pins B10/TX, B11/RX on port A
void PeripheralInit_USART3(){

    GPIO_InitTypeDef GPIO_InitDef;
    USART_InitTypeDef USART_InitDef;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOB, ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

    GPIO_StructInit(&GPIO_InitDef);

    // initialize B10/TX
    GPIO_InitDef.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitDef.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitDef.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitDef);

    // initialize B11/RX
    GPIO_InitDef.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitDef.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitDef.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitDef);

    USART_StructInit(&USART_InitDef);
    // modify for non-default values
    USART_InitDef.USART_BaudRate = 115200*2; //Clock is half what I expected???
    USART_InitDef.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_InitDef.USART_WordLength = USART_WordLength_8b;
    USART_InitDef.USART_StopBits = USART_StopBits_1;
    USART_InitDef.USART_Parity = USART_Parity_No;
    USART_InitDef.USART_HardwareFlowControl = USART_HardwareFlowControl_None;

    USART_Init(USART3, &USART_InitDef);
    USART_Cmd(USART3, ENABLE);
}


// transmit a character over USART1 A9/TX
int put_char_usart3(int c) {
    while(USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);
    USART3->DR = (c & 0xFF);
    return 0;
}

// receive a character over USART1 A10/RX
int get_char_usart3(){

    // this while loop makes the call blocking
    // will wait until byte received
    // while(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET);
    // return USART1->DR & 0xFF;

    if (USART_GetFlagStatus(USART3, USART_FLAG_RXNE) != RESET){
        return USART3->DR & 0xFF;
    } else {
        return -1;
    }
}

// transmit a string over USART1 A9/TX
int serial_print_usart3(char chars[]){
    int i=0;
    // print until null char or until too many characters counted
    while((chars[i] != 0x00) && (i < 512)){
        put_char_usart3(chars[i]);
        i++;
    }
    if(i < 512){ return 0; } else { return 1; }
}


void SPI2_Slave_Setup() {
	GPIO_InitTypeDef GPIO_InitDef;
	SPI_InitTypeDef SPI_InitDef;

	//Init Structs
	GPIO_StructInit(&GPIO_InitDef);
	SPI_StructInit(&SPI_InitDef);

	// initialize clocks
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

	//Enable SPI2 Interrupt
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = SPI2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0; //High priority
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// initialize PB15/MOSI alternate function open-drain (10 MHz)
	GPIO_InitDef.GPIO_Pin = GPIO_Pin_15;
	GPIO_InitDef.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_InitDef.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOB, &GPIO_InitDef);

	// initialize PB14/MISO alternate function push-pull (10 MHz)
	GPIO_InitDef.GPIO_Pin = GPIO_Pin_14;
	GPIO_InitDef.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitDef.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOB, &GPIO_InitDef);

	// initialize PB13/SCK alternate function open-drain (10 MHz)
	GPIO_InitDef.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitDef.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_InitDef.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOB, &GPIO_InitDef);

	// initialize PB12/SS alternate function open-drain (10 MHz)
	GPIO_InitDef.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitDef.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_InitDef.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOB, &GPIO_InitDef);


	// initialize SPI slave
	// for slave, no need to define SPI_BaudRatePrescaler
	SPI_InitDef.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitDef.SPI_Mode = SPI_Mode_Slave;
	SPI_InitDef.SPI_DataSize = SPI_DataSize_8b; // 8-bit transactions
	SPI_InitDef.SPI_FirstBit = SPI_FirstBit_MSB; // MSB first
	SPI_InitDef.SPI_CPOL = SPI_CPOL_Low; // CPOL = 0, clock idle low
	SPI_InitDef.SPI_CPHA = SPI_CPHA_2Edge; // CPHA = 1
	SPI_InitDef.SPI_NSS = SPI_NSS_Hard; // use hardware SS
	//SPI_InitDef.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32; // APB1 36/32 = 1.125 MHz
	// SPI_InitDef.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128; // APB1 36/128 = 0.28 MHz
	// SPI_InitDef.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8; // APB1 36/8 = 4.5 MHz
	//SPI_InitDef.SPI_CRCPolynomial = 7;
	SPI_CalculateCRC(SPI2, DISABLE);
	SPI_Init(SPI2, &SPI_InitDef);

	SPI_Cmd(SPI2, ENABLE);

	SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_RXNE, ENABLE);
	SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_TXE, ENABLE);
}

void SPI2_IRQHandler(void) {
	static uint8_t count = 0;
	uint8_t* data = (uint8_t*)ServoValues; //Byte pointer

	if (SPI_I2S_GetITStatus(SPI2, SPI_I2S_IT_TXE) == SET) {
		SPI2->DR = 0xff; //Send
	}
	if (SPI_I2S_GetITStatus(SPI2, SPI_I2S_IT_RXNE) == SET) {
		Recv = true;

		//rx = SPI2->DR;
		data[count] = SPI2->DR;

		count++;


		if(count == 20*2) {
			count = 0;
		}
	}
}


int main(void) {

	// Configure SysTick Timer
	SysTick_Config(SystemCoreClock / 1000);

	//Initialise sane servo Values
	for(uint8_t i = 0; i < 20; i++) {
		ServoValues[i] = 24000; //1ms
	}

	GPIO_Setup();
	Timer_Setup();
	SPI2_Slave_Setup();
	PeripheralInit_USART3();

	//JTAG-DP Disabled, SW-DP Enabled to enable PA15, PB3, PB4
	//(See Reference Manual RM0008 page 177)
	AFIO->MAPR &= 0xF8FFFFFF; //Clear bits
	AFIO->MAPR |= 0x02000000; //Set bits

	serial_print_usart3("STM32 UP!\r\n");
	while(1) {

		#ifdef DEBUG_UART
		char str[128];
		sprintf(str, "Time %ld\r\n", Milliseconds);
		if(Recv) {
			serial_print_usart3("\n\n\rSPI\r\n");
			for(uint8_t i = 0; i < 20; i++) {
				sprintf(str, "%02x=%hu\r\n", i, ServoValues[i]);
				serial_print_usart3(str);
			}
			Recv = false;
		}
		#endif
	}


}

void SysTick_Handler (void){ //ISR Cortex M3 Tick
	Milliseconds++;
}

#ifdef USE_FULL_ASSERT
void assert_failed ( uint8_t * file , uint32_t line)
{
/* Infinite loop */
/* Use GDB to find out why we're here */
while (1);
}
#endif