// Hexapod Servo Code for STM32F030C8T6
// D.McCarthy, 11/19, 12/19

//#define STM32F030x8
#include "stm32f0xx.h"

//Servo Globals
static volatile uint16_t ServoValues[20];
static volatile uint8_t RxPacketBuffer[42]; //Incoming Packet Buffer
static volatile uint8_t TxPacketADC[6];     //6 bytes line up nicely with 42 byte
static volatile uint16_t ServoAMask = 0x0000;
static volatile uint16_t ServoBMask = 0x0000;
static volatile bool RxFlag = false;

extern "C" {
	void TIM3_IRQHandler(void);
    void SPI2_IRQHandler(void);
}

void PopulateServo() {
    for(uint8_t i = 0; i < 20; i++) {
        ServoValues[i] = 36000; //1.5ms - Neutral Position
    }

    TxPacketADC[7] = 0xfe;
    TxPacketADC[8] = 0xed;
}


//Clock Configuration
void ClockInit(void) {
  RCC->CR |= RCC_CR_CSSON | RCC_CR_HSEON; //Start HSE Clock

  //Setup PLL
  RCC->CFGR = RCC->CFGR &(~RCC_CFGR_PLLMUL)|(RCC_CFGR_PLLMUL6);
  RCC->CFGR |= RCC_CFGR_PLLSRC | RCC_CFGR_PPRE_DIV2; //PLLSRC from HSE, APB Clock is PLL/2

  RCC->CR |= RCC_CR_PLLON; //Turn PLL on
  
  FLASH->ACR |= 0b001;//FLASH_ACR_LATENCY -- One wait state, if 24 MHz < SYSCLK ≤ 48 MHz
  RCC->CFGR |= RCC_CFGR_SW_PLL; //Switch Sysclock to PLL

  //RCC->CFGR |= RCC_CFGR_MCO_SYSCLK; //Enable system clock output on MCO for Debuging
}



//Initialise GPIO Pins
void GPIOInit(void) {
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN;

    //PA0 - PA8, PA10,PA11 Output
    GPIOA->MODER |= GPIO_MODER_MODER0_0 | GPIO_MODER_MODER1_0 | GPIO_MODER_MODER2_0 | GPIO_MODER_MODER3_0
                  |GPIO_MODER_MODER4_0 | GPIO_MODER_MODER5_0 | GPIO_MODER_MODER6_0 | GPIO_MODER_MODER7_0
                  |GPIO_MODER_MODER8_0 | GPIO_MODER_MODER11_0 | GPIO_MODER_MODER12_0;
    
    //PB3-PB11 Output
    GPIOB->MODER |= GPIO_MODER_MODER3_0 | GPIO_MODER_MODER4_0 | GPIO_MODER_MODER5_0 | GPIO_MODER_MODER6_0
                  |GPIO_MODER_MODER7_0 | GPIO_MODER_MODER8_0 | GPIO_MODER_MODER9_0 | GPIO_MODER_MODER10_0
                  |GPIO_MODER_MODER11_0;
}


//SPI2 Slave Configuration
void SPI2Init(void) {
    NVIC_SetPriority(SPI2_IRQn,1); //Highest priority, so TIM3 doesn't intefere
    NVIC_EnableIRQ(SPI2_IRQn);

    //Setup DMA Ch 5 for SPI2_TX
    RCC->AHBENR |= RCC_AHBENR_DMA1EN; //Clock enable
    SPI2->CR2 |= SPI_CR2_TXDMAEN;     //TXDMA enable

    //DMA Config
    DMA1_Channel5->CPAR = (uint32_t) (&(SPI2->DR)); //Peripheral Adderess
    DMA1_Channel5->CMAR = (uint32_t) (&TxPacketADC[0]);        //Memory Address
    DMA1_Channel5->CNDTR = 6; // 4 *2 bytes
    DMA1_Channel5->CCR &= DMA_CCR_MSIZE_0 & ~DMA_CCR_MSIZE_1 | DMA_CCR_PSIZE_0 & ~DMA_CCR_PSIZE_1; //8 bits M & P Size
    DMA1_Channel5->CCR |= DMA_CCR_DIR | DMA_CCR_MINC | DMA_CCR_CIRC; //Read from memory, circular, incrementing
    DMA1_Channel5->CCR |= DMA_CCR_EN;

    //Set GPIO mode to alternate mode for SPI2 pins
    GPIOB->MODER |= GPIO_MODER_MODER12_1 | GPIO_MODER_MODER13_1 | GPIO_MODER_MODER14_1 | GPIO_MODER_MODER15_1;

    //Enable clock to SPI2
    RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;
    
    //Setup Interrupt, Mode/etc //SPI_CR2_TXEIE
    SPI2->CR2 = SPI_CR2_RXNEIE | SPI_CR2_FRXTH | SPI_CR2_DS_2 | SPI_CR2_DS_1 | SPI_CR2_DS_0 | SPI_CR2_TXDMAEN; //RXNE IUnterrupt, 8-bit Rx fifo
    SPI2->CR1 |= SPI_CR1_SPE | SPI_CR1_CPHA; //Enable SPI2, Capture on falling edge CPHA = 1, CPOL = 0
}

void SPI2_IRQHandler(void) {
    static uint8_t count = 0;
	uint8_t* data = (uint8_t*)ServoValues; //Byte pointer

	if (SPI2->SR & SPI_SR_RXNE) {
		RxPacketBuffer[count] = SPI2->DR;
        count++;
        if(count == 42) { //42 bytes Rx Buffer
			    count = 0;
                RxFlag = true;
		}
    }
}



void TIM3_IRQHandler(void) {
    static uint8_t ServoIndex = 0;

    if(TIM3->SR & TIM_SR_UIF) { // if UIF flag is set
        TIM3->SR &= ~TIM_SR_UIF; // clear UIF flag
        
        //Turn Servo pin on at Index for Port A and B
        GPIOA->ODR |= 0x01ff & (0x1 << ServoIndex) & ServoAMask; //On
        GPIOB->ODR |= 0x0ff8 & (0x1 << ServoIndex+3) & ServoBMask; //On

        //Output Compare
        TIM3->CCR1 = ServoValues[ServoIndex];
        TIM3->CCR2 = ServoValues[ServoIndex+10];

        ServoIndex++;
        if(ServoIndex > 8) { // 400Hz/8 = 50Hz //500Hz/10 = 50Hz
            ServoIndex = 0;
        }
    }


    if(TIM3->SR & TIM_SR_CC1IF) { // if UIF flag is set
        TIM3->SR &= ~TIM_SR_CC1IF; // clear UIF flag
        GPIOA->ODR &= 0xfe00; //Off
    }


    if(TIM3->SR & TIM_SR_CC2IF) { // if UIF flag is set
        TIM3->SR &= ~TIM_SR_CC2IF; // clear UIF flag
        GPIOB->ODR = 0x0000; //Off
    }
	
}

//TIM3 Configuration
void TimerInit() {
    RCC->APB1ENR |= RCC_APB1ENR_TIM3EN; //Enable TIM3 Clock

    //Enable TIM3 Interrupt
    NVIC_SetPriority(TIM3_IRQn,0);
    NVIC_EnableIRQ(TIM3_IRQn);


    TIM3->PSC = 1; //Clock Prescaler : 24Mhz = SYSCLK/2
    TIM3->ARR = 60000; //2.5ms 400Hz //48000; //Count Reset value for 500Hz (2ms) 500Hz

    //Enable CC1, CC2 Interrupts and Trigger Interrupt
    TIM3->DIER |= TIM_DIER_CC1IE | TIM_DIER_CC2IE | TIM_DIER_UIE;

    //Output Compare
    TIM3->CCR1 = 24000;
    TIM3->CCR2 = 24000;


    TIM3->CR1 |= TIM_CR1_CEN; //Enable TIM1 in Up Mode
    
}

void ADCInit() {
    //Set PB0, PB1 as Analog
    GPIOB->MODER |= GPIO_MODER_MODER0_0 | GPIO_MODER_MODER0_1 | GPIO_MODER_MODER1_0 | GPIO_MODER_MODER1_1;
    
    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;

    RCC->CR2 |= RCC_CR2_HSI14ON; //Turn on 14Mhz HSI clock for ADC
    //Wait for HSI14
    while((RCC->CR2 & RCC_CR2_HSI14RDY) == 0) {
        GPIOA->BSRR |= GPIO_BSRR_BR_11;
        GPIOA->BSRR |= GPIO_BSRR_BS_11;
    }

    ADC1->CFGR2 &= ~ADC_CFGR2_CKMODE; // Set clock source to HSI14

    ADC1->CR |= ADC_CR_ADCAL;
    //Wait for ADC calibration
    while(ADC1->CR & ADC_CR_ADCAL == 1) {
        GPIOA->BSRR |= GPIO_BSRR_BR_11;
        GPIOA->BSRR |= GPIO_BSRR_BS_11;
    }

    ADC1->CFGR1 |= ADC_CFGR1_DMAEN | ADC_CFGR1_DMACFG | ADC_CFGR1_CONT;  //DMA Circular Mode
    ADC1->CFGR1 &= ~ADC_CFGR1_DISCEN;  //Not Discontinuous Mode
    ADC1->CFGR1 &= ~ADC_CFGR1_RES_0 & ~ADC_CFGR1_RES_1; // 12-bits

    //ADC_IN8 //Batt Voltage
    //ADC_IN9 //Batt Current
    ADC1->CHSELR |= ADC_CHSELR_CHSEL8 | ADC_CHSELR_CHSEL9; //Select ADC1 Channels to sample on
    ADC1->SMPR |= ADC_SMPR_SMP_0 | ADC_SMPR_SMP_1 | ADC_SMPR_SMP_2; //Set Sampling time 239.5 ADC Clk ~58Khz


    //Setup DMA
    DMA1_Channel1->CPAR  = (uint32_t) (&(ADC1->DR));
    DMA1_Channel1->CMAR  = (uint32_t) (TxPacketADC);
    DMA1_Channel1->CNDTR = 2; //Two Channels
    DMA1_Channel1->CCR |= DMA_CCR_MINC | DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0 | DMA_CCR_CIRC;//DMA_CCR_TEIE
    DMA1_Channel1->CCR |= DMA_CCR_EN;

    
    ADC1->CR |= ADC_CR_ADEN; //Enable ADC1
    //Wait for ADC to become ready
    while((ADC1->ISR & ADC_ISR_ADRDY) == 0) {
        GPIOA->BSRR |= GPIO_BSRR_BR_11;
        GPIOA->BSRR |= GPIO_BSRR_BS_11;
    }

    ADC1->CR |= ADC_CR_ADSTART;
}
    
    

//Handles Packet
void HandlePacket() {
    GPIOA->BSRR |= GPIO_BSRR_BS_11;
    uint8_t opcode = RxPacketBuffer[0]; //Byte 0 is Header
    uint8_t args   = RxPacketBuffer[1]; //Byte 1 is args
    uint16_t* RxPtr = (uint16_t*)RxPacketBuffer;

    switch (opcode) {
    case 0x01: //Set Servos
        for (uint8_t i = 0; i < 20; i++) {
            ServoValues[i] = RxPtr[i+1];
        }
        break;
    case 0x02: //Enable Servos
        ServoAMask = RxPtr[1];
        ServoBMask = RxPtr[2];
        break;
    case 0x04:
        GPIOA->ODR = GPIOA->ODR;
        break;
    default:
        break;
    }

    RxFlag = false;
    GPIOA->BSRR |= GPIO_BSRR_BR_11;
}

int main (void) {
    ClockInit();
    GPIOInit();
    GPIOA->BSRR |= GPIO_BSRR_BS_12;
    GPIOA->BSRR |= GPIO_BSRR_BS_11;
    ADCInit();
    PopulateServo();
    TimerInit();
    SPI2Init();
    GPIOA->BSRR |= GPIO_BSRR_BR_12;
    GPIOA->BSRR |= GPIO_BSRR_BR_11; 

    for(;;) {
        if(RxFlag) {
            HandlePacket();
        }
    }

    // Return 0 to satisfy compiler
    return 0;
}