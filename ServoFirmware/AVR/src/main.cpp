/*
Daniel McCarthy - 10/10/19
This code generates 18 servo signals from an ardino nano board using Timer 1
Need to avoid PC5 and PC4 since they are for I2C, they are disabled here, otherwise this could do 20 signals.

Signal Assignments:
ID Pin Label Signal?
00 PB0 D8    Y         //A
01 PB1 D9    Y
02 PB2 D10   Y
03 PB3 D11   Y
04 PB4 D12   Y
05 PB5 D13   Y
06 PC0 A0    Y
07 PC1 A1    Y
08 PC2 A2    Y
09 PC3 A3    Y
10 PD0 RX0   N (Need to disable USB UART and use ICSP)  //B
11 PD1 TX1   N
12 PD2 D2    Y
13 PD3 D3    Y
14 PD4 D4    Y
15 PD5 D5    Y
16 PD6 D6    Y
17 PD7 D7    Y
18 (Unused)
19 (Unused)
*/

#include <Arduino.h>

//Global
uint16_t ServoValues[20];

int main() {
  //Initialise timing array
  for(uint8_t i = 0; i < 20; i++) {
    ServoValues[i] = 16000;
  }

  //Pin Direction Setup
  DDRB |= 0x3f; //Avoid PB6+
  DDRC |= 0x0f; //Avoid PC4,PC5,PC6
  DDRD |= 0xff; //Have full port

  cli(); //Disable interrupts

  //Timer 1 Setup (16-bit)
  TCCR1A = 0x00; //Waveform Generation Mode Registers set for CTC
  TCCR1B = 0x19; //CTC Mode (12) using ICR1 as TOP, No Clock prescaling, so 1ms = 16,000 cycles

  //Set TOP value = 32,000 = 0x7d00 = 2ms
  ICR1H = 0x7d;
  ICR1L = 0x00;

  //Set interrupt mask
  TIMSK1 = 0b00100110;

  sei(); //Enable interrupts

  //I2C Setup



  for(;;) {
  }
}

ISR(TIMER1_CAPT_vect){
  static uint8_t ServoIndex = 0;

  //Set Outputs for A

  if(ServoIndex < 6) {
    PORTB |= 0x3f & (0x01 << ServoIndex);//6 //A0-5
  } else {
    PORTC |= 0x0f & (0x01 << (ServoIndex-6));//6 //A6-9
    //PORTC |= 0x0f;// & (0x01 << 0);//6 //A6-9
  }


  //Set Outputs for B
  PORTD |= 0xff & (0x01 << ServoIndex);//8 //B0-7

  //Set new timings for A
  OCR1AH = uint8_t((ServoValues[ServoIndex] & 0xff00) >> 8);
  OCR1AL = uint8_t(ServoValues[ServoIndex]);

  //Set new timings for B
  OCR1BH = uint8_t((ServoValues[ServoIndex+10] & 0xff00) >> 8);
  OCR1BL = uint8_t(ServoValues[ServoIndex+10]);

  //Increment Servo index
  ServoIndex = ServoIndex < 9 ? ServoIndex+1 : 0; //10*2ms = 20ms = 50Hz
}


ISR(TIMER1_COMPA_vect){
  PORTB &= ~0x3f;//A0-6
  PORTC &= ~0x0f;//A7-10
}

ISR(TIMER1_COMPB_vect){
  PORTD &= ~0xff;//B0-6
}