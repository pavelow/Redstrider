// SPI Test Code
// Daniel McCarthy 12/19

#include <wiringPiSPI.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

#include <stdint.h>
#include <stdlib.h>
#include <time.h>

void delay(uint16_t millis) {
	clock_t start = clock();
	while(clock() < (start + millis*1000)) {
	}
}

typedef struct SPIpacket {
	uint8_t opcode;
	uint8_t arg;
	uint16_t Data[20];
} SPIpacket;

typedef struct PowerData {
	float I;
	float V;
	float P;
} PowerData;

void Initialize(uint32_t baud) {
	int fd = wiringPiSPISetup(0, baud);
	/* Data on rising edge is apparently unreliable, set data on falling edge
	 * STM32F103 CPOL = 0, CPHA = 1
	 * BCM2835_SPI_MODE1 CPOL = 0, CPHA = 1
	 */
	int spiMode = 1;
	ioctl(fd, SPI_IOC_WR_MODE, &spiMode);
}

SPIpacket BuildPacket(uint8_t opcode, uint8_t arg) {
	SPIpacket pkt;
	pkt.opcode = opcode;
	pkt.arg = arg;

	//Populate Data with Sane Values
	for( uint8_t i = 0; i< 20; i++) {
		pkt.Data[i] = 36000; //1.5ms PWM
	}

	return pkt;
}

int SendData(SPIpacket *pkt) {
	wiringPiSPIDataRW(0, (uint8_t*)pkt, sizeof(SPIpacket));
	return 0;
}

PowerData ParsePowerData(SPIpacket *pkt) {
	//Grab single values
	float avgV = pkt->Data[3];
	float avgI = pkt->Data[2];
	
	//Convert to V
	avgV = (avgV/4096*3.3)*4.1333;      //V from divider
	avgI = ((avgI/4096*3.3)/200)/0.001; //I = V/R

	PowerData p;
	p.I = avgI;
	p.V = avgV;
	p.P = avgI*avgV;
	return p;
}

SPIpacket EnableServo(uint8_t n) {
	//Enable Servo n
	SPIpacket m0 = BuildPacket(0x02, 0x00); //0x02 = Servo Mask
	m0.Data[0] = 0x0000;
	m0.Data[1] = 0x0000;
	if(n < 9) {
		m0.Data[0] = 0b1 << n; //0x01ff
	} else if(n < 19) {
		m0.Data[1] = 0b1 << n-7; //0x0ff8
	} else {
		printf("Index out of range");
	}
	return m0;
}


//TODO: add calibration values to this
//Accepts angle in radians, sets timing based on calibration
void SetServoAngle(uint8_t idx, float angle, SPIpacket *pkt) {
	//48000 = 2ms
	if(angle > 0.0f && angle < M_PI) {
		pkt->Data[idx] = 24000 + 24000*(angle/M_PI); //Set Value from Console
	}
}

//TODO: load calibration file
void LoadCalibration() {
	return;
}


int main(int argc, char ** argv) {
	Initialize(125000); //Init SPI @ 125kbps
	SPIpacket m0;

	//Get Args
	uint16_t idx = 0;
	uint16_t a = 36000;
	if(argc > 2) {
		a = atoi(argv[1]);
		idx = atoi(argv[2]);
	}

	//Enable Servo
	m0 = EnableServo(idx);
	SendData(&m0);

	

	//Move Servo
	m0 = BuildPacket(0x01, 0x00); //0x02 = Servo Mask
	m0.Data[idx] = a; //Set Value from Console
	
	SendData(&m0);

	PowerData p = ParsePowerData(&m0);
	printf("%fV @ %fA = %fW\n", p.V, p.I, p.P);
	
	// for(uint8_t i = 0; i< 20; i++) {
	// 	printf("%04x\n", m0.Data[i]);
	// }

	
}