import IK
from math import *
import vrep

ClientID = 0
LegP = [0,0,0,0,0,0]
LegH = [0,0,0,0,0,0]
LegK = [0,0,0,0,0,0]

Rconversion = pi/180

#Initialize protocol
def init():
    #Connect to Server
    ClientID = vrep.simxStart("10.0.2.4", 45566, False, False, 500, 5)
    print("Connected: {0}".format(ClientID))
    #Get Handles for all the joints
    for i in range(6):
        print("Getting Handle: {0}/5".format(i))
        LegP[i] = vrep.simxGetObjectHandle(ClientID, "Pelvis_{0}#".format(i), vrep.simx_opmode_blocking)
        LegH[i] = vrep.simxGetObjectHandle(ClientID, "ip_{0}#".format(i), vrep.simx_opmode_blocking)
        LegK[i] = vrep.simxGetObjectHandle(ClientID, "Knee_{0}#".format(i), vrep.simx_opmode_blocking)

#Angle Offset function, because I imported the model with a non-0 pose. won't do that again...
def pOffset(n):
    if(n == 0):
        return -42.62
    elif(n == 1):
        return -54.58
    elif(n == 2):
        return 3.86
    elif(n == 3):
        return -1.94
    elif(n == 4):
        return 48.06
    elif(n == 5):
        return 38.12
    else:
        return 0

#Because some of the leg joints are flipped :/
def Flip(n):
    if(n == 0 or n == 2):
        return -1
    else:
        return 1
def PFlip(n):
    if(n == 4):
        return -1
    else:
        return 1

def send(Hexapod):
    for Leg in Hexapod.Legs:
        #print "Sending L{3} P:{0} H:{1} K:{2}".format(Leg.Ta *Rconversion - pi/2, Leg.Ha *Rconversion - pi/2, Leg.Ka *Rconversion - pi/2, Leg.index)
        vrep.simxSetJointTargetPosition(ClientID, LegP[Leg.index][1], PFlip(Leg.index)*Flip(Leg.index)*((Leg.Ta+pOffset(Leg.index)) *Rconversion - pi/2) , vrep.simx_opmode_streaming)
        vrep.simxSetJointTargetPosition(ClientID, LegH[Leg.index][1], Flip(Leg.index)*((Leg.Ha-31.87) *Rconversion - pi/2) , vrep.simx_opmode_streaming)
        vrep.simxSetJointTargetPosition(ClientID, LegK[Leg.index][1], Flip(Leg.index)*((180-Leg.Ka+31.87) *Rconversion - pi/2) , vrep.simx_opmode_streaming)

def get(Hexapod):
    for Leg in Hexapod.Legs:
        Leg.Ta = vrep.simxGetJointPosition(ClientID, LegP[Leg.index][1], vrep.simx_opmode_streaming)[1]/Rconversion
        Leg.Ha = vrep.simxGetJointPosition(ClientID, LegH[Leg.index][1], vrep.simx_opmode_streaming)[1]/Rconversion
        Leg.Ka = vrep.simxGetJointPosition(ClientID, LegK[Leg.index][1], vrep.simx_opmode_streaming)[1]/Rconversion
