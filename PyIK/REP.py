import IK
from math import *
import VProtocol as Protocol #We can swap this out with whatever interface we want
import time
import pygame

t = 0
pygame.init()
Protocol.init()  #Init Protocol
screen = pygame.display.set_mode((640, 480))
screen.fill((0,0,0))
Hexapod = IK.Hexapod(6,8) #Init Hexapod IK height, len
Hexapod.SetOffsets()

#For Gait
def Clamp(n):
	if n > 0:
		return 0
	else:
		return n

Z1 = 0
Z0 = 0
X1 = 0
X0 = 0
dt = 0.05
scaleFactor = 30.0
WFlag = True

def ToZ(n, s, t):
	if n > t:
		n -= s*(n-t)
	elif n < t:
		n += s*(t-n)
	return n

while 1:
	mouse = pygame.mouse.get_pos()
	screen.fill((0,0,0))
	for event in pygame.event.get():
		if event.type in (pygame.QUIT, pygame.KEYDOWN):
			sys.exit()
	
	Xm = ((mouse[0]-320)/scaleFactor)
	Ym = ((mouse[1]-240)/scaleFactor)
	#print "Xm{0} Ym{1}".format(Xm, Ym)

	#Walks in circles forever.
	#Hexapod.TransRot([0,3,4], 0, Clamp(sin(t+pi)), cos(t+pi)-1 ,0, cos(t+pi)/2,0)
	#Hexapod.TransRot([1,2,5], 0, Clamp(sin(t))   , cos(t)   -1 ,0, cos(t)   /2,0)
	#Hexapod.Translate([0,1,2,3,4,5], 0,0,sin(t)*3)
	#Hexapod.TransRot([0,1,2,3,4,5], sin(t*4),sin(t)+1,sin(t*5), cos(t*3)/8,0,sin(t*2)/8)

	#Hexapod.TransRot([0,1,2,3,4,5], 0,0,0, 0,cos(t/4)/8,0)

	#Hexapod.Translate([0,1,2,3,4,5], 0,0,cos(t/2))

	YOffset = 0.3
	ZIncr = dt/10
	XIncr = dt/10
	VCoeff = 5

	XV = Xm*2
	ZV = Ym*2
	XZV = sqrt(pow(XV,2) + pow(ZV,2))/3

	#Y0 = Clamp(sin(t)+YOffset)*2
	#Y1 = Clamp(sin(t+pi)+YOffset)*2

	## Ideas for decoupled method.
	#D0 = sqrt(pow(X0,2) + pow(Z0,2))
	#D1 = sqrt(pow(X1,2) + pow(Z0,2))
	#Y0 = -cos(D0/1) if WFlag else 0  
	#Y1 = -cos(D1/1) if not WFlag else 0
	
	print "Y0{0} Y1{1}".format(Y0,Y1)

	
	
	if(XZV > 0):
		X0 = (X0+(VCoeff*XV*XIncr) if(Y0 > -0.4) else ToZ(X0, XZV*dt, -XV/XZV))
		X1 = (X1+(VCoeff*XV*XIncr) if(Y1 > -0.4) else ToZ(X1, XZV*dt, -XV/XZV))

		Z0 = (Z0+(VCoeff*ZV*ZIncr) if(Y0 > -0.4) else ToZ(Z0, XZV*dt, -ZV/XZV))
		Z1 = (Z1+(VCoeff*ZV*ZIncr) if(Y1 > -0.4) else ToZ(Z1, XZV*dt, -ZV/XZV))

	#print "{0} {1}".format(X0,Z0)
	print

	#Hexapod.printCoords()

	Hexapod.Translate([0,3,4], X0, Y0 ,Z0)
	Hexapod.Translate([1,2,5], X1, Y1 ,Z1)

	Protocol.send(Hexapod)
	t -= dt*XZV

	pygame.draw.aaline(screen, (255,255,255), (0,240), (640,240), 4)
	pygame.draw.aaline(screen, (255,255,255), (320,0), (320,480), 4)
	pygame.display.update()

#	time.sleep(0.01) #Or else it runs too fast :P