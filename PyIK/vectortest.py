#! /usr/bin/python3

import numpy as np
from vector import Vector3

aV = Vector3(1,2,3)
bV = Vector3(4,5,6)

a = np.array((1,2,3))
b = np.array((4,5,6))

print("\nCross Product")
print(np.cross(a,b))
print(aV.cross(bV))

print("\nDot Product")
print(np.dot(a,b))
print(aV.dot(bV))

print("\nLength")
print(np.linalg.norm(a))
print(aV.length())

print("\nDistance")
print(np.linalg.norm(a - b))
print(aV.distance(bV))
