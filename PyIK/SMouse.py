import socket
import pygame
import time
import struct

#UDP_IP = "192.168.8.254"
UDP_IP = "10.0.2.3"
UDP_PORT = 25602

screen = pygame.display.set_mode((640, 640))
screen.fill((0,0,0))
scaleFactor = 100.0

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while 1:
	mouse = pygame.mouse.get_pos()
	screen.fill((0,0,0))
	for event in pygame.event.get():
		if event.type in (pygame.QUIT, pygame.KEYDOWN):
			sys.exit()
	
	Xm = ((mouse[0]-320)/scaleFactor)
	Ym = ((mouse[1]-320)/scaleFactor)
	
	print "{0}, {1}".format(Xm, Ym)

	buf = struct.pack('i f f', 1, Xm, Ym)
	#buf[0] =
	#buf[1] = struct.pack('f',Ym)

	sock.sendto(buf, (UDP_IP, UDP_PORT))

	pygame.draw.aaline(screen, (255,255,255), (0,320), (640,320), 4)
	pygame.draw.aaline(screen, (255,255,255), (320,0), (320,640), 4)
	pygame.display.update()

	#time.sleep(0.001) #Or else it runs too fast :P