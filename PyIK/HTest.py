#Test routines for Hexapod
from math import *
import time

class Test:
    def __init__(self, dt, h, p):
        self.dt = dt
        self.p = p
        self.h = h


    def Tz(self):
        t = 0.0
        while 1:
            self.h.Translate([0,1,2,3,4,5], 0,0,sin(t)*2)
            self.p.send(self.h)
            t+= self.dt

    def Tx(self):
        t = 0.0
        while 1:
            self.h.Translate([0,1,2,3,4,5], sin(t)*2,0,0)
            self.p.send(self.h)
            t+= self.dt

    def Ty(self):
        t = 0.0
        while 1:
            self.h.Translate([0,1,2,3,4,5], 0,sin(t),0)
            self.p.send(self.h)
            t+= self.dt

    def Tc(self):
        t = 0.0
        while 1:
            self.h.Translate([0,1,2,3,4,5], sin(t)*2,0,cos(t)*2)
            self.p.send(self.h)
            t+= self.dt