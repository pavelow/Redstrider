#Protocol for sending servo positions
#D4 is TX on NodeMCU

from machine import UART
import IK

class protocol:
	def __init__(self):
		self.uart = UART(1,9600)
		self.uart.init(9600, bits=8, parity=None, stop=1)

	def init(self):
		pass

	def send(self,Hexapod):
		#Hexapod is a class with a list of legs in it
		self.uart.write(bytes([255]))
		#self.uart.write(bytes([255]))
		for Leg in Hexapod.Legs:
			self.uart.write(bytes([int(Leg.Ka), int(Leg.Ha), int(Leg.Ta)]))
		self.uart.write(bytes([254]))
