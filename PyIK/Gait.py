import IK
from math import *
import VProtocol as Protocol #We can swap this out with whatever interface we want
import time
import pygame

t = 0
pygame.init()
Protocol.init()  #Init Protocol
screen = pygame.display.set_mode((640, 480))
screen.fill((0,0,0))
Hexapod = IK.Hexapod(6,8) #Init Hexapod IK height, len
Hexapod.SetOffsets()

#For Gait
def Clamp(n):
	if n > 0:
		return 0
	else:
		return n

Z1 = 0
Z0 = 0
X1 = 0
X0 = 0
dt = 0.05
scaleFactor = 30.0 #for mouse
WFlag = True
t = 0

def ToZ(n, s, t): #Approach Value??? Can't remember
	if n > t:
		n -= s*(n-t)
	elif n < t:
		n += s*(t-n)
	return n

def walk(xV,zV,yRot, t):
	YOffset = 0.3   #Amount to offset Sin(t) by
	ZIncr = dt/10   #Z increment, mult by Z Vel input
	XIncr = dt/10   #X increment, mult by X Vel input
	VCoeff = 5

	XV = Xm
	ZV = Ym
	XZV = sqrt(pow(XV,2) + pow(ZV,2))/3 #Total Translation Velocity

	Y0 = Clamp(sin(t)+YOffset)*2        #Y set 0
	Y1 = Clamp(sin(t+pi)+YOffset)*2     #Y set 1

	## Ideas for decoupled method.
	#D0 = sqrt(pow(X0,2) + pow(Z0,2))
	#D1 = sqrt(pow(X1,2) + pow(Z0,2))
	#Y0 = -cos(D0/1) if WFlag else 0
	#Y1 = -cos(D1/1) if not WFlag else 0

	#print "Y0{0} Y1{1}".format(Y0,Y1)

	if(XZV > 0):
		X0 = (X0+(VCoeff*XV*XIncr) if(Y0 > -0.4) else ToZ(X0, XZV*dt, -XV/XZV))
		X1 = (X1+(VCoeff*XV*XIncr) if(Y1 > -0.4) else ToZ(X1, XZV*dt, -XV/XZV))

		Z0 = (Z0+(VCoeff*ZV*ZIncr) if(Y0 > -0.4) else ToZ(Z0, XZV*dt, -ZV/XZV))
		Z1 = (Z1+(VCoeff*ZV*ZIncr) if(Y1 > -0.4) else ToZ(Z1, XZV*dt, -ZV/XZV))

	#print "{0} {1}".format(X0,Z0)
	print

	#Hexapod.printCoords()

	Hexapod.Translate([0,3,4], X0, Y0 ,Z0)
	Hexapod.Translate([1,2,5], X1, Y1 ,Z1)

	Protocol.send(Hexapod) #Send joint angles
	t -= dt*XZV



while 1:
	#pygame Stuff
	mouse = pygame.mouse.get_pos()
	screen.fill((0,0,0))
	for event in pygame.event.get():
		if event.type in (pygame.QUIT, pygame.KEYDOWN):
			sys.exit()

	pygame.draw.aaline(screen, (255,255,255), (0,240), (640,240), 4)
	pygame.draw.aaline(screen, (255,255,255), (320,0), (320,480), 4)
	pygame.display.update()

	Xm = ((mouse[0]-320)/scaleFactor)
	Ym = ((mouse[1]-240)/scaleFactor)
	#Have control inputs.
	walk(Xm, Ym, 0, t) #Call to walk

	time.sleep(0.01) #Or else it runs too fast :P
