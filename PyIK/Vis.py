#! /bin/python
#This file is for visulising IK.py functions in 2D As a method of debuging and verification

import os, sys
import pygame
import IK
from pygame.locals import *
from math import *
import LProtocol #We can swap this out

Protocol = LProtocol.protocol()

Width = 1024
Height = 768

Leg = IK.Leg(0)

t = 0

print("PyGame Init")

pygame.init()
pygame.font.init()
screen = pygame.display.set_mode((Width, Height))
screen.fill((0,0,0))
#scaleFactor = 15.0
scaleFactor = 20

Leg = IK.Leg(0)
Protocol.init()


while 1:
	for event in pygame.event.get():
		if event.type in (QUIT, KEYDOWN):
			sys.exit()
	screen.fill((0,0,0))
	mouse = pygame.mouse.get_pos()
	print(mouse)
	t += 0.05
	#Leg.Set3D(11+(cos(t)*2),6+(sin(t)*2), 0)
	#Leg.Set3D(4+(cos(t)*8),6, 0)
	Xm = -((mouse[0]-(Width/2))/scaleFactor)
	Ym = ((mouse[1]-(Height/2))/scaleFactor)
	print("Xm{0} Ym{1}".format(Xm, Ym))

	Leg.Set3D(Xm, Ym, cos(t/2)*4)

	#for Leg in Hexapod.Legs:
	#	Leg.Set3D(Xm, Ym, 0)
	#Protocol.send(Hexapod)

	#Limit angles
	if Leg.Ha > 180: Leg.Ha = 180
	if Leg.Ha < 0: Leg.Ha = 0

	Hl = Leg.Hl #Hip Length
	Kl = Leg.Kl #Knee Length
	Tl = Leg.Tl #Tibea Length

	Ha = (-Leg.Ha+90) * pi/180
	Ka = (Leg.Ka) * pi/180


	#Calculate Coords
	Hx = cos(Ha)*Hl*scaleFactor #Hip X
	Hy = sin(Ha)*Hl*scaleFactor #Hip Y
	Kx = (cos(Ka+Ha)*Kl*scaleFactor) #Knee X
	Ky = (sin(Ka+Ha)*Kl*scaleFactor) #Knee y

	#print cos(Leg.Ta * pi/180)
	CTa = cos((Leg.Ta - 90) * pi/180)
	STa = sin((Leg.Ta - 90) * pi/180)

	Tx = CTa*Leg.Tl*scaleFactor
	Ty = STa*Leg.Tl*scaleFactor
	TKA = Hx+Kx
	#print(TKA)
	TKx = CTa*TKA
	TKy = STa*TKA

	#Visulise Joints
	pygame.draw.circle(screen, (128,255,255), (int(Width/2),int(Height/2)), 3)  #Hip Joint
	pygame.draw.circle(screen, (255,128,255), (int((Width/2)+Tl*scaleFactor),int(Height/2)), 3) #Hip -> Femur Joint

	pygame.draw.circle(screen, (255,255,128), (int((Width/2)+Tl*scaleFactor+Hx),int((Height/2)+Hy)), 3) #Femur -> Tibea Jouint

	pygame.draw.circle(screen, (255,128,128), (int((Width/2)+Tl*scaleFactor+Hx+Kx),int((Height/2)+Hy+Ky)), 3) #End Effector

	#Visulise Bounds
	pygame.draw.circle(screen, (255,0,0), (int((Width/2)+Tl*scaleFactor),int((Height/2)+Hl*scaleFactor)), int((Kl)*scaleFactor)+2,2)
	pygame.draw.rect(screen, (0,255,0), [Width,Height,int(int(Width/2)), 0])
	pygame.draw.circle(screen, (255,0,0), (int((Width/2)+Tl*scaleFactor),int(Height/2)), int((Kl+Hl)*scaleFactor+2),2)
	pygame.draw.circle(screen, (64,0,0), (int((Width/2)+Tl*scaleFactor),int(Height/2)), int((Kl-Hl)*scaleFactor),2)
	pygame.draw.circle(screen, (255,0,0), (int((Width/2)+Tl*scaleFactor),int((Height/2)-Hl*scaleFactor)), int((Kl)*scaleFactor)+2,2)
	pygame.draw.line(screen, (255,0,0), (int(Width/2),0), (int(Width/2),Height))
	pygame.draw.rect(screen, (0,0,0), [0,0,int((Width/2)), Height])
	#Draw Leg Side
	pygame.draw.aaline(screen, (128,255,255), (int(Width/2),int(Height/2)), (int(Width/2)+Tl*scaleFactor,int(Height/2)), 4) #Thigh
	pygame.draw.aaline(screen, (255,128,255), (int(Width/2)+Tl*scaleFactor,int(Height/2)), (int(Width/2)+Tl*scaleFactor+Hx,int(Height/2)+Hy), 4) #Hip
	pygame.draw.aaline(screen, (255,255,128), (int(Width/2)+Tl*scaleFactor+Hx,int(Height/2)+Hy), (int(Width/2)+Tl*scaleFactor+Hx+Kx,int(Height/2)+Hy+Ky), 4) #Knee

	#Draw Leg Top View (Left of screen)
	pygame.draw.aaline(screen, (128,255,255), (0,int(Height/2)), (0+Tx,int(Height/2)+Ty), 4)
	pygame.draw.aaline(screen, (255,128,255), (0+Tx,int(Height/2)+Ty), (0+Tx+TKx,int(Height/2)+Ty+TKy), 4)
	pygame.draw.circle(screen, (255,128,255), (int(0+Tx+TKx),int((Height/2)+Ty+TKy)), 3)
	pygame.draw.aaline(screen, (255,255,255), (0+Tx+TKx, 0), (0+Tx+TKx, Height), 4)

	#Draw Dl
	pygame.draw.aaline(screen, (255,0,0), (int(Width/2)+Tl*scaleFactor,int(Height/2)), (int(Width/2)+Tl*scaleFactor+Hx+Kx,int(Height/2)+Hy+Ky), 4)
	#Draw angles
	font = pygame.font.Font(None, 20)
	txt = font.render(str(Leg.Ta), 1, (128,255,255))
	#screen.blit(txt, ((Width/2)-10,(Height/2)+10))
	txt = font.render(str(Leg.Ha), 1, (255,128,255))
	#screen.blit(txt, (int((Width/2)+Tl*10)-10,(Height/2)+10))
	txt = font.render(str(Leg.Ka), 1, (255,255,128))
	#screen.blit(txt, (int((Width/2)+Tl*10+Hx)-10,int((Height/2)+Hy)-10))

	#txt = font.render(str("X{0} Y{1} Z{2}".format(round(Xm,2), round(Ym,2), round(cos(t)*4),2)), 1, (255,255,255))
	txt = font.render(str("X{0} Y{1} Z{2}".format(round(Leg.X,2), round(Leg.Y,2), round(Leg.Z),2)), 1, (255,255,255))
	screen.blit(txt, (100,100))
	txt = font.render(str("X{0} Y{1} Z{2}".format(round(Xm,2), round(Ym,2), 0,2)), 1, (255,255,255))
	screen.blit(txt, (100,120))
	txt = font.render(str("X{0} Y{1} Z{2}".format(round((Tl*scaleFactor+Hx+Kx)/scaleFactor,2), round((Hy+Ky)/scaleFactor,2), 0,2)), 1, (255,255,255))
	screen.blit(txt, (100,140))
	txt = font.render(str("X{0} Y{1} Z{2}".format((-round(Tl*scaleFactor+Hx+Kx,2)/scaleFactor) - Xm, (round(Hy+Ky,2)/scaleFactor) - Ym, 0,2)), 1, (255,255,255))
	screen.blit(txt, (100,160))


	pygame.display.update()
