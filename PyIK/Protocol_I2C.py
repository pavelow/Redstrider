#Protocol for sending servo positions over I2C
#D4 is TX on NodeMCU

from machine import I2C, Pin
import IK

class protocol:
	def __init__(self):
		self.i2c = I2C(scl=Pin(5), sda=Pin(4), freq=100000) #100k baud

	def send(self,Hexapod):
		buf = bytearray(18)
		#Hexapod is a class with a list of legs in it)
		for Leg in Hexapod.Legs:
			buf[Leg.index*3] = int(Leg.Ka)
			buf[Leg.index*3+1] = int(Leg.Ha)
			buf[Leg.index*3+2] = int(Leg.Ta)
		self.i2c.writeto(0x08, buf)