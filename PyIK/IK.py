#Port of IK from C to Python, the idea is to now run IK on the ESP8266 so I can update the code quickly/etc
from math import *

class Leg:
	def __init__(self, num):
		self.index = num
	#Lengths in CM
	Kl = 9.45 #9.45 with feet   #9.1 Without feet
	Hl = 4.312
	Tl = 4.1
	Dl = 0
	KneeOffsetAngle = -atan(2.05/Kl)/(pi/180)
	XOffset = 0.0
	YOffset = 0.0
	ZOffset = 0.0

	#Home Coordinates (All Transformations are relative to these)
	XH = 0.0
	YH = 0.0
	ZH = 0.0

	#Actual Coordinates which are to be computed
	X = 0.0
	Y = 0.0
	Z = 0.0

	#Angles in degrees, gives initial pose
	Ka = 90.0 #Knee Angle
	Ha = 90.0 #Hip Angle
	Ta = 90.0 #Pelvic Angle

	#Calibration Vars in Degrees
	KaCal = 0
	HaCal = 0
	PaCal = 0

	def Set2D(self, X, Y): #Calculate Knee and Hip Angle (on a 2D Plane)
		if(X != 0):
			self.Da = atan(Y/X)                 #Angle to point
		else:
			self.Da = pi
		self.Dl = sqrt(pow(X,2) + pow(Y,2)) #Distance to point
		#print self.Da

		#Angles, by law of cosines
		cKa = (pow(self.Hl,2) + pow(self.Kl,2) - pow(self.Dl,2))/(2*self.Hl*self.Kl)
		if(abs(cKa) <= 1): # not Impossible K Position
			self.Ka = 180 - (acos(cKa) * 180/pi)
		#Validate Ha
		cHa = (pow(self.Hl,2) + pow(self.Dl,2) - pow(self.Kl,2))/(2*self.Hl*self.Dl)
		if (abs(cHa) <= 1): # not Impossible H Position
			if(X > 0):
				self.Ha = 90 + ((acos(cHa) -self.Da) * 180/pi)
			elif(X == 0):
				self.Ha = ((acos(cHa) -self.Da) * 180/pi)
			else:
				self.Ha = ((acos(cHa) -self.Da) * 180/pi) -90
			self.Ha += self.KneeOffsetAngle


	def Set3D(self, X,Y,Z): #Calculate Angles for 3D Coords
		#Convert to local Coords
		self.X = X
		self.Y = Y
		self.Z = Z
		X = X - self.XOffset
		Y = Y - self.YOffset
		Z = Z - self.ZOffset
		#Invert Coords

		#print "IK{0} X:{1} Y:{2} Z:{3}".format(self.index, X,Y,Z)

		#Z *= -1 # I have no idea why... For 8266

		if(self.index % 2 == 0): #If Leg is odd, it's on the other side, so we need to invert some stuff
			X *= -1
		else:
			Z *= -1


		theta = 0
		if( X !=0):
			theta = atan(Z/X) 	#Angle Compensated
		leng  = sqrt(pow(Z,2) + pow(X,2)) 	#Leng Compensated
		self.Ta = 90+(theta * 180/pi)
		self.Set2D((1 if (X > 0) else -1)*leng-self.Tl, Y)


class Hexapod:
	def __init__(self, Height, LegLen):
		self.LegLen = LegLen #Initial Distance of leg from Body
		self.Height = Height #Initial Height
		self.SetOffsets()
	#Leg 0, 1 are front
	Legs = []
	for i in range(6):
		Legs.append(Leg(i))

	def Translate(self, LegList, X, Y, Z):
		self.TransRot(LegList, X,Y,Z,0,0,0)

	def Rotate(self, LegList, AngleX, AngleY, AngleZ):
		self.TransRot(LegList, 0,0,0, AngleX, AngleY, AngleZ)

	def TransRot(self, LegList, X,Y,Z,AngleX, AngleY, AngleZ):
		for Leg in LegList:

			#YRot
			YX = (self.Legs[Leg].XH*cos(AngleY) - self.Legs[Leg].ZH * sin(AngleY))
			YZ = (self.Legs[Leg].XH*sin(AngleY) + self.Legs[Leg].ZH * cos(AngleY))
			YY = self.Legs[Leg].YH

			#XRot
			XX = YX
			XZ = (YY*sin(AngleX) + YZ * cos(AngleX))
			XY = (YY*cos(AngleX) - YZ * sin(AngleX))

			#ZRot
			ZX = (XY*sin(AngleZ) + XX * cos(AngleZ))
			ZZ = XZ
			ZY = (XY*cos(AngleZ) - XX * sin(AngleZ))

			#Just average them, don't care that much
			self.Legs[Leg].X = ZX + X
			self.Legs[Leg].Y = ZY + Y
			self.Legs[Leg].Z = ZZ + Z

		#print "TransRotate: X{0} Y{1} Z{2}".format(AngleX, AngleY, AngleZ)
		self.Compute()

	def Compute(self):
		for Leg in self.Legs:
			Leg.Set3D(Leg.X, Leg.Y, Leg.Z)

	#def printCoords(self):
	#	for Leg in self.Legs:
	#		print "L{0} X:{1} Y:{2} Z:{3}".format(Leg.index, Leg.X, Leg.Y, Leg.Z)

	def SetOffsets(self): #Sets the offsets so that Legs use Global Coordinates (Measurements from CAD)
		for i in self.Legs:
			if(i.index % 2 > 0):
				i.XOffset = 2.156
			else:
				i.XOffset = -2.156
			i.YOffset = 0
		self.Legs[0].ZOffset = -5.28
		self.Legs[1].ZOffset = -5.28

		self.Legs[4].ZOffset = 5.28
		self.Legs[5].ZOffset = 5.28

		#Set Home positions
		LegLen = self.LegLen #6#8#4.206 #7

		self.Legs[2].XH = -LegLen + self.Legs[2].XOffset
		self.Legs[3].XH = LegLen + self.Legs[3].XOffset

		self.Legs[0].XH = self.Legs[0].XOffset - LegLen*0.707
		self.Legs[1].XH = self.Legs[1].XOffset + LegLen*0.707
		self.Legs[0].ZH = self.Legs[0].ZOffset - LegLen*0.707
		self.Legs[1].ZH = self.Legs[1].ZOffset - LegLen*0.707

		self.Legs[4].XH = self.Legs[4].XOffset - LegLen*0.707
		self.Legs[5].XH = self.Legs[5].XOffset + LegLen*0.707
		self.Legs[4].ZH = self.Legs[4].ZOffset + LegLen*0.707
		self.Legs[5].ZH = self.Legs[5].ZOffset + LegLen*0.707

		for Leg in self.Legs:
			Leg.YH = self.Height #~Gives us the largest XZ plane movement
