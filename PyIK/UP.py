#Control of Hexapod from network
import usocket as socket
import ustruct
from math import *


class NetControl():
    def __init__(self,h,p):
        self.h = h
        self.p = p

    t = 0
    Y1 = 0
    Y0 = 0
    Z1 = 0
    Z0 = 0
    X1 = 0
    X0 = 0
    dt = 0.05


    def Listen(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        ai = socket.getaddrinfo("0.0.0.0", 25602)
        addr = ai[0][-1]
        sock.bind(addr)
        while 1:
            data, addr = sock.recvfrom(256)
            a = ustruct.unpack('i f f', data) # X, ?, Y
            #h.Translate([0,1,2,3,4,5],a[0],0,a[2])
            print(a)


def Walk(self,X,Z):
    YOffset = 0.3
    ZIncr = dt/10
    XIncr = dt/10
    VCoeff = 5
    XV = X*2
    ZV = Z*2
    XZV = sqrt(pow(XV,2) + pow(ZV,2))/3
    Y0 = Clamp(sin(t)+YOffset)*2
    Y1 = Clamp(sin(t+pi)+YOffset)*2

    if(XZV > 0):
        X0 = (X0+(VCoeff*XV*XIncr) if(Y0 > -0.4) else ToZ(X0, XZV*dt, -XV/XZV))
        X1 = (X1+(VCoeff*XV*XIncr) if(Y1 > -0.4) else ToZ(X1, XZV*dt, -XV/XZV))

        Z0 = (Z0+(VCoeff*ZV*ZIncr) if(Y0 > -0.4) else ToZ(Z0, XZV*dt, -ZV/XZV))
        Z1 = (Z1+(VCoeff*ZV*ZIncr) if(Y1 > -0.4) else ToZ(Z1, XZV*dt, -ZV/XZV))
        
    self.h.Translate([0,3,4], X0, Y0 ,Z0)
    self.h.Translate([1,2,5], X1, Y1 ,Z1)
    self.p.send(h)
    t += dt*XZV


def Clamp(n):
	if n > 0:
		return 0
	else:
		return n

def ToZ(n, s, t):
	if n > t:
		n -= s*(n-t)
	elif n < t:
		n += s*(t-n)
	return n