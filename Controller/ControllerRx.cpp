#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 

#define PORT 42424
#define MAXLINE 1024

typedef struct rxmsg{
    float lx;
    float ly;
    float rx;
    float ry;
}rxmsg;


float ReverseFloat( const float inFloat )
{
   float retVal;
   char *floatToConvert = ( char* ) & inFloat;
   char *returnFloat = ( char* ) & retVal;

   // swap the bytes into a temporary buffer
   returnFloat[0] = floatToConvert[3];
   returnFloat[1] = floatToConvert[2];
   returnFloat[2] = floatToConvert[1];
   returnFloat[3] = floatToConvert[0];

   return retVal;
}



int main(int argc, char const *arv[]) {
    int sockfd;
    char buffer[MAXLINE];
    char *hello = "Hello from Server";
    struct sockaddr_in servaddr, cliaddr;

    //Creating socket file descriptor
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket creation failure");
        exit(EXIT_FAILURE);
    }

    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(PORT);

    if( bind(sockfd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    //Should be bound now


    for(;;) {
            unsigned int len, n;
            len = sizeof(cliaddr);

            n = recvfrom(sockfd, (char *)buffer, MAXLINE, MSG_WAITALL, (struct sockaddr *) &cliaddr, &len);
            //buffer[n] = '\0';
            rxmsg* b = (rxmsg*)&buffer;
            //printf("Rx : %f\n", b->lx);
            printf("Rx : %f\n", ReverseFloat(b->lx));
    }

    // sendto(sockfd, (const char *)hello, strlen(hello), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);
    // printf("Hello message sent.\n");

    return 0;
}
