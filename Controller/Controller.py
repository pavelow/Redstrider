#! /usr/bin/python

import struct, socket, sys
from time import sleep
from xbox360controller import Xbox360Controller



if __name__ == "__main__":
    #Init Controller
    c = Xbox360Controller(0, axis_threshold=0.2)

    #Init UDP
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    UDP_IP1 = "127.0.0.1"
    UDP_IP = "192.168.4.1"
    UDP_PORT = 42424


    while(True):
        fmt = "!ffff"
        lx = ly = rx = ry = 0.0

        if(abs(c.axis_l.x) > 0.1):
            lx = c.axis_l.x
        if(abs(c.axis_l.y) > 0.1):
            ly = c.axis_l.y
        if(abs(c.axis_r.x) > 0.1):
            rx = c.axis_r.x
        if(abs(c.axis_r.y) > 0.1):
            ry = c.axis_r.y

        msg = struct.pack(fmt, lx, ly, rx, ry)
        sock.sendto(msg, (UDP_IP, UDP_PORT))
        sock.sendto(msg, (UDP_IP1, UDP_PORT))
        print("send")
        sleep(0.02) #sleep 50ms
